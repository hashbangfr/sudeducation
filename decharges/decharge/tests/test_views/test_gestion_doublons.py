import pytest
from django.urls import reverse

from decharges.decharge.models import TempsDeDecharge, UtilisationTempsDecharge
from decharges.decharge.views.gestion_doublons import get_decharge_doublons
from decharges.parametre.models import ParametresDApplication
from decharges.user_manager.models import Syndicat

pytestmark = pytest.mark.django_db


def test_get_doublons():
    federation = Syndicat.objects.create(
        is_superuser=True, email="admin@example.com", username="Fédération"
    )
    ParametresDApplication.objects.create(
        annee_en_cours=2022,
    )
    syndicat = Syndicat.objects.create(
        email="syndicat1@example.com", username="Syndicat 1"
    )
    TempsDeDecharge.objects.create(
        syndicat_beneficiaire=syndicat,
        syndicat_donateur=federation,
        annee=2020,
        temps_de_decharge_etp=10,
    )
    doublon_1 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
    )
    # doublon UAI différent
    doublon_2 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="1313131D",
        syndicat=syndicat,
    )
    # doublon prenom différent
    doublon_3 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="POLAN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )
    # doublon nom different
    doublon_4 = UtilisationTempsDecharge.objects.create(
        nom="MICHEL",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )
    # Non doublon
    non_doublon = UtilisationTempsDecharge.objects.create(
        nom="LANDRY",
        prenom="Pierre",
        code_etablissement_rne="2654262E",
        syndicat=syndicat,
        supprime_a=None,
    )
    result = get_decharge_doublons()
    assert result[0] == doublon_1
    assert result[1] == doublon_2
    assert result[2] == doublon_4
    assert result[3] == doublon_3
    assert non_doublon not in result


def test_identifier_doublons(client):
    federation = Syndicat.objects.create(
        is_superuser=True, email="admin@example.com", username="Fédération"
    )
    ParametresDApplication.objects.create(
        annee_en_cours=2022,
    )
    syndicat = Syndicat.objects.create(
        email="syndicat1@example.com", username="Syndicat 1"
    )
    TempsDeDecharge.objects.create(
        syndicat_beneficiaire=syndicat,
        syndicat_donateur=federation,
        annee=2020,
        temps_de_decharge_etp=10,
    )
    client.force_login(federation)
    response = client.get(reverse("decharge:identifier_doublons"))
    assert len(response.context["doublons"]) == 0

    doublon_1 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
    )
    # doublon UAI différent
    doublon_2 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="1313131D",
        syndicat=syndicat,
    )
    # doublon prenom différent
    UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="POLAN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )
    # doublon nom different
    UtilisationTempsDecharge.objects.create(
        nom="MICHEL",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )

    response = client.get(reverse("decharge:identifier_doublons"))
    assert len(response.context["doublons"]) == 4

    response = client.post(
        reverse("decharge:identifier_doublons"),
        {
            "doublon_1": doublon_1.pk,
            "doublon_2": doublon_2.pk,
        },
    )
    assert response.status_code == 302
    response = client.get(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_1.pk, "pk2": doublon_2.pk},
        )
    )


def test_fusionner_doublons(client):
    federation = Syndicat.objects.create(
        is_superuser=True, email="admin@example.com", username="Fédération"
    )
    ParametresDApplication.objects.create(
        annee_en_cours=2022,
    )
    syndicat = Syndicat.objects.create(
        email="syndicat1@example.com", username="Syndicat 1"
    )
    TempsDeDecharge.objects.create(
        syndicat_beneficiaire=syndicat,
        syndicat_donateur=federation,
        annee=2020,
        temps_de_decharge_etp=10,
    )

    doublon_1 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
    )
    # doublon UAI différent
    doublon_2 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="MARTIN",
        code_etablissement_rne="1313131D",
        syndicat=syndicat,
    )
    # doublon prenom différent
    doublon_3 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="POLAN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )
    # doublon nom different
    doublon_4 = UtilisationTempsDecharge.objects.create(
        nom="MICHEL",
        prenom="MARTIN",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )

    client.force_login(federation)

    response = client.get(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_1.pk, "pk2": doublon_2.pk},
        )
    )
    assert response.status_code == 200

    response = client.post(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_1.pk, "pk2": doublon_2.pk},
        ),
        {"first_modify": ""},
    )
    assert response.status_code == 302
    assert response.url == f"/modifier-beneficiaire/{doublon_1.pk}/"
    doublon_2.refresh_from_db()
    assert doublon_2.supprime_a is not None

    response = client.post(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_1.pk, "pk2": doublon_3.pk},
        ),
        {"second_not_modify": ""},
    )
    assert response.status_code == 302
    assert response.url == reverse("decharge:identifier_doublons")
    doublon_1.refresh_from_db()
    assert doublon_1.supprime_a is not None

    response = client.post(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_3.pk, "pk2": doublon_4.pk},
        ),
        {"imprevu": ""},
    )
    assert response.status_code == 400
    doublon_4.refresh_from_db()
    assert doublon_4.supprime_a is None

    response = client.post(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_3.pk, "pk2": doublon_4.pk},
        ),
        {"first_not_modify": ""},
    )
    assert response.status_code == 302
    assert response.url == reverse("decharge:identifier_doublons")
    doublon_4.refresh_from_db()
    assert doublon_4.supprime_a is not None

    doublon_5 = UtilisationTempsDecharge.objects.create(
        nom="MICHELLE",
        prenom="ELODIE",
        code_etablissement_rne="2626262D",
        syndicat=syndicat,
        supprime_a=None,
    )
    response = client.post(
        reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": doublon_3.pk, "pk2": doublon_5.pk},
        ),
        {"second_modify": ""},
    )
    assert response.status_code == 302
    assert response.url == f"/modifier-beneficiaire/{doublon_5.pk}/"
    doublon_3.refresh_from_db()
    assert doublon_3.supprime_a is not None
