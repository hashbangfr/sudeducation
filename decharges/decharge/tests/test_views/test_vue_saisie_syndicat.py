import pytest
from django.urls import reverse

from decharges.decharge.models import UtilisationTempsDecharge
from decharges.parametre.models import ParametresDApplication
from decharges.user_manager.models import Syndicat

pytestmark = pytest.mark.django_db


def test_vue_saisie_syndicat(client):
    syndicat = Syndicat.objects.create(
        email="syndicat@example.com", username="Syndicat"
    )
    federation = Syndicat.objects.create(
        is_superuser=True, email="admin@example.com", username="Fédération"
    )
    ParametresDApplication.objects.create(
        annee_en_cours=2021, decharges_editables=False
    )
    client.force_login(federation)
    temps_utilises = UtilisationTempsDecharge.objects.create(
        civilite="M.",
        prenom="Foo",
        nom="BAR",
        heures_de_decharges=40,
        heures_d_obligation_de_service=1607,
        code_etablissement_rne="1234567A",
        annee=2021,
        syndicat=syndicat,
    )

    response = client.get(
        reverse(
            "decharge:index-syndicat",
            kwargs={"pk": syndicat.pk},
        )
    )

    assert response.status_code == 200
    assert response.context["temps_utilises_par_syndicat"]["Syndicat"] == [
        temps_utilises
    ]
