from django import forms


class IdentifierDoublonsForm(forms.Form):
    doublon_1 = forms.Field(widget=forms.HiddenInput())
    doublon_2 = forms.Field(widget=forms.HiddenInput())
