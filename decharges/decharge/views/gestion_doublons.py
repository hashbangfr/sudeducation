from datetime import datetime
from functools import reduce
from operator import or_

from django import http
from django.db.models import Count, Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView

from decharges.decharge.forms.gestion_doublons_form import IdentifierDoublonsForm
from decharges.decharge.mixins import CheckConfigurationMixin, FederationRequiredMixin
from decharges.decharge.models import UtilisationTempsDecharge


def get_decharge_doublons():
    beneficiaires_actifs = UtilisationTempsDecharge.objects.filter(
        supprime_a__isnull=True
    )

    nom_prenom_dupliques = (
        beneficiaires_actifs.values("nom", "prenom")
        .annotate(count=Count("pk"))
        .order_by("prenom", "nom")
        .filter(count__gt=1)
    )
    if nom_prenom_dupliques.exists():
        doublons_nom_prenom = beneficiaires_actifs.filter(
            reduce(
                or_, [Q(nom=v["nom"], prenom=v["prenom"]) for v in nom_prenom_dupliques]
            )
        )
    else:
        doublons_nom_prenom = UtilisationTempsDecharge.objects.none()

    prenom_rne_dupliques = (
        beneficiaires_actifs.values("code_etablissement_rne", "prenom")
        .annotate(count=Count("pk"))
        .order_by("prenom", "code_etablissement_rne")
        .filter(count__gt=1, supprime_a__isnull=True)
    )
    if prenom_rne_dupliques.exists():
        doublons_prenom_rne = beneficiaires_actifs.filter(
            reduce(
                or_,
                [
                    Q(
                        prenom=v["prenom"],
                        code_etablissement_rne=v["code_etablissement_rne"],
                    )
                    for v in prenom_rne_dupliques
                ],
            )
        )
    else:
        doublons_prenom_rne = UtilisationTempsDecharge.objects.none()

    nom_rne_dupliques = (
        beneficiaires_actifs.values("code_etablissement_rne", "nom")
        .annotate(count=Count("pk"))
        .order_by("nom", "code_etablissement_rne")
        .filter(count__gt=1, supprime_a__isnull=True)
    )
    if nom_rne_dupliques.exists():
        doublons_nom_rne = beneficiaires_actifs.filter(
            reduce(
                or_,
                [
                    Q(nom=v["nom"], code_etablissement_rne=v["code_etablissement_rne"])
                    for v in nom_rne_dupliques
                ],
            )
        )
    else:
        doublons_nom_rne = UtilisationTempsDecharge.objects.none()

    return doublons_nom_prenom.union(doublons_prenom_rne, doublons_nom_rne)


class IdentifierDoublons(CheckConfigurationMixin, FederationRequiredMixin, FormView):
    """Vue pour identifier les doublons pour les utilisations
    de temps de décharges. Sont considérées doublons potentiels :
    - Deux entrées avec Prénom + nom identique, RNE différent
    - Prénom + RNE identiques, Nom différent
    - Nom + RNE identiques, Prénom différent.
    """

    template_name = "decharge/identifier_doublons.html"
    form_class = IdentifierDoublonsForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["doublons"] = get_decharge_doublons()
        return context

    def form_valid(self, form):
        self.doublon_pk1 = form.cleaned_data["doublon_1"]
        self.doublon_pk2 = form.cleaned_data["doublon_2"]
        return super().form_valid(form)

    def get_success_url(self):
        return reverse(
            "decharge:fusionner_doublons",
            kwargs={"pk1": self.doublon_pk1, "pk2": self.doublon_pk2},
        )


class FusionnerDoublons(CheckConfigurationMixin, FederationRequiredMixin, TemplateView):
    template_name = "decharge/fusionner_doublons.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["doublon_1"] = get_object_or_404(
            UtilisationTempsDecharge, pk=self.kwargs["pk1"]
        )
        context["doublon_2"] = get_object_or_404(
            UtilisationTempsDecharge, pk=self.kwargs["pk2"]
        )
        return context

    def post(self, request, *args, **kwargs):
        if "first_modify" in self.request.POST:
            self.modification_souhaitee = True
            self.doublon_a_garder = self.kwargs["pk1"]
            self.doublon_a_supprimer = self.kwargs["pk2"]
        elif "first_not_modify" in self.request.POST:
            self.modification_souhaitee = False
            self.doublon_a_garder = self.kwargs["pk1"]
            self.doublon_a_supprimer = self.kwargs["pk2"]
        elif "second_modify" in self.request.POST:
            self.modification_souhaitee = True
            self.doublon_a_garder = self.kwargs["pk2"]
            self.doublon_a_supprimer = self.kwargs["pk1"]
        elif "second_not_modify" in self.request.POST:
            self.modification_souhaitee = False
            self.doublon_a_garder = self.kwargs["pk2"]
            self.doublon_a_supprimer = self.kwargs["pk1"]
        else:
            return http.HttpResponseBadRequest(
                "Erreur : l'option demandée n'est pas disponible"
            )
        beneficiaire_a_supprimer = get_object_or_404(
            UtilisationTempsDecharge, pk=self.doublon_a_supprimer
        )
        beneficiaire_a_supprimer.supprime_a = datetime.now()
        beneficiaire_a_supprimer.save()
        if self.modification_souhaitee:
            return redirect(
                reverse(
                    "decharge:modifier_beneficiaire",
                    kwargs={"pk": self.doublon_a_garder},
                )
            )
        return redirect(reverse("decharge:identifier_doublons"))
