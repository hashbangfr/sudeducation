from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.urls import reverse
from django.views.generic import CreateView, UpdateView

from decharges import settings
from decharges.decharge.forms import UtilisationCreditDeTempsSyndicalPonctuelForm
from decharges.decharge.mixins import CheckConfigurationMixin, CheckTempsEditableMixin
from decharges.decharge.models import UtilisationCreditDeTempsSyndicalPonctuel
from decharges.decharge.views.utils import calcul_repartition_temps


class CTSCreate(
    CheckConfigurationMixin, LoginRequiredMixin, CheckTempsEditableMixin, CreateView
):
    template_name = "decharge/cts_form.html"
    form_class = UtilisationCreditDeTempsSyndicalPonctuelForm

    def get_success_url(self):
        messages.success(self.request, "CTS enregistré avec succès !")
        return reverse("decharge:index")

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs["syndicat"] = self.request.user
        form_kwargs["annee"] = self.params.annee_en_cours
        form_kwargs["federation"] = self.federation
        return form_kwargs


class CTSUpdate(
    CheckConfigurationMixin, LoginRequiredMixin, CheckTempsEditableMixin, UpdateView
):
    template_name = "decharge/cts_form.html"
    form_class = UtilisationCreditDeTempsSyndicalPonctuelForm
    model = UtilisationCreditDeTempsSyndicalPonctuel
    context_object_name = "cts"

    def get_object(self, queryset=None):
        """vérification que l'utilisateur a bien les permissions pour éditer cet objet"""
        cts = super().get_object(queryset=queryset)
        if cts.syndicat != self.request.user and not self.request.user.is_federation:
            raise Http404()
        return cts

    def get_success_url(self):
        messages.success(self.request, "CTS mis à jour avec succès !")
        return reverse("decharge:index")

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs["syndicat"] = self.object.syndicat
        form_kwargs["annee"] = self.object.annee
        form_kwargs["federation"] = self.federation
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        annee_en_cours = self.params.annee_en_cours
        (
            cts_consommes,
            temps_decharge_federation,
            temps_donnes,
            temps_donnes_total,
            temps_recus_par_des_syndicats,
            temps_recus_par_la_federation,
            temps_restant,
            temps_utilises,
            temps_utilises_total,
        ) = calcul_repartition_temps(annee_en_cours, self.federation, self.request.user)

        total_demi_journees_non_consommees = (
            (temps_restant * settings.NB_HOURS_IN_A_YEAR) / 7 * 2
        )

        context.update(
            {
                "total_etp_non_consommes": round(temps_restant, settings.PRECISION_ETP),
                "total_demi_journees_non_consommees": round(
                    total_demi_journees_non_consommees, settings.PRECISION_ETP
                ),
            }
        )

        return context
